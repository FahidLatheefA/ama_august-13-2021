# Ask Me Anything (13-August-2021)

## What are different types of network Topology?
1. **Bus topology**
is the network topology where every node (every device on the network) is connected to a solo main cable line. Data is transmitted in a single route, from one point to the other.

![Bus Topology Image](images/bus.png)

2. **Ring Topology**
Ring Topology is a topology type in which every computer is connected to another computer on each side. The last computer is connected to the first, thus forming a ring shape. This topology allows for each computer to have exactly two neighbouring computers.

![Ring Topology Image](images/ring.png)

3. **Star Topology**
Star Topology is the network topology in which all the nodes are connected via cables to a single node called a hub, which is the central node.

![Star Topology Image](images/star.png)

4. **Mesh Topology**
Mesh topology is the kind of topology in which all the nodes are connected with all the other nodes via a network channel. It has n(n-1)/2 network channels to connect n nodes.

![Mesh Topology Image](images/mesh.png)

5. **Tree Topology**
Tree topology is the topology in which the nodes are connected hierarchically, with all the nodes connected to the topmost node or root node. Hence, it is also known as hierarchical topology. Tree topology has at least three levels of hierarchy.

![Tree Topology Image](images/tree.png)

6. **Hybrid Topology**
Hybrid Topology comprises of two or more different types of topologies. It is a reliable and scalable topology, but simultaneously, it is a costly one. It receives the merits and demerits of the topologies used to build it.

![Hybrid Topology Image](images/hybrid.png)

## Why `if __name__ == '__main__':` is used in python?
Anything that comes inside `if __name__ == '__main__':` will be run only when we explicitly run that particular file. In short, this allows us to have standalone files. Now if we call this module in another file, nothing under `if __name__ == '__main__':` will be called.

Consider this example,
```python
if __name__ == '__main__':
    print 'This program is being run by itself'
else:
    print 'I am being imported from another module'
```
If we run this program from the file where the code exists, we will get an output `"This program is being run by itself"`. If we are importing this module and running it elswewhere, we get an output `"I am being imported from another module"`.


## What is an abstract class? Why do we need it?
In Python, **abstract base classes** provide a blueprint for concrete classes. They don’t contain implementation. Instead, they provide an interface and make sure that derived concrete classes are properly implemented.
-   Abstract base classes cannot be instantiated. Instead, they are inherited and extended by the concrete subclasses.
-   Subclasses derived from a specific abstract base class must implement the methods and properties provided in that abstract base class. Otherwise, an error is raised during the object instantiation.

We use an abstract class as a **template** and according to the need, we extend it and build on it.

## Abstraction and Inheritance in Python?
**Abstraction** in Python is the process of hiding the real implementation of an application from the user and emphasizing only on usage of it. For example, consider you have bought a new electronic gadget. Along with the gadget, you get a user guide, instructing how to use the application, but this user guide has no info regarding the internal working of the gadget.
Through the process of abstraction in Python, a programmer can hide all the irrelevant data/process of an application in order to reduce complexity and increase efficiency.
**Inheritance** is the capability of one class to derive or inherit the properties from another class. 
It can be used for representing real-world relationships. Moreover, it helps in making our code re-usable without repeating.

## What is Deadlock in Operating System? How do you resolve it? What is Starvation?
A process in operating system uses resources in the following way.  
1) Requests a resource  
2) Use the resource  
3) Releases the resource

**Deadlock** is a situation where a set of processes are blocked because each process is holding a resource and waiting for another resource acquired by some other process.

![Deadlock](images/deadlock.png)

There are three ways to handle deadlock
1. Deadlock prevention
Prevent the deadlock from happening.
2. Deadlock detection and recovery
Allow deadlock to occur, then do preemption to handle it once occurred.
3. Ignore the problem altogether
Ignore the problem and reboot your system to get away from the deadlock.

**Starvation** is the problem that occurs when high priority processes keep executing and low priority processes get blocked for indefinite time. Starvation can be resolved using **Aging**, where the priority of long waiting process is gradually increased.

## Difference between centralized and distributed version control system?

**Centralized** Version Control Systems keep the history of changes on a central server from which everyone requests the latest version of the work and pushes the latest changes to. This means that everyone sharing the server also shares everyone’s work. Eg: [Sourceforge](Sourceforge.net)

In **Distributed** Version Control System, everyone has a local copy of the entire work’s history. This means that it is not necessary to be online to change revisions or add changes to the work. “Distributed” comes from the fact that there isn’t a central entity in charge of the work’s history, so that anyone can sync with any other team member. Eg: [GitLab](https://about.gitlab.com/)

### Differences
|  Centralized|Distributed  |
|--|--|
|  **Easier to learn** and setup|Confusing to learn and set up  |
|  Not possible to work offline|Possible to work **offline**  |
|  Slower, due to the communication with central server|**Faster**  |
|  History difficult to access because of communication with the central server|Entire **history** is easily accessible  |

## In distributed systems, what is fragmentation and replication?

Data **fragmentation** is a technique used to break up objects. In designing a distributed database, you must decide which portion of the database is to be stored where. One technique used to break up the database into logical units called fragments. Fragmentation information is stored in a distributed data catalogue which the processing computer uses to process a user's request. The 3 types of fragmentation are Horizontal, Mixed and Vertical Fragmentation.

Data replication is the storage of data copies at multiple sites on the network. Fragment copies can be stored at several site, thus enhancing data availability and response time. Replicated data is subject to a **mutual consistency rule**. This rule requires that all copies of the data fragments must be identical and to ensure data consistency among all of the replications. A database can be either fully replicated, partially replicated or unreplicated.

## Why there are Lists and Arrays?
List allows the elements to be **heterogenous**, i.e a single list is allowed to have different data types.
Array is **homogenous** and all the elements have the same datatype, for example: float. It is specifically created for doing arithmetic operatios.

Moreover, arrays can store data very compactly and are more efficient for storing large amounts of data. Arrays are great for numerical operations; lists cannot directly handle math operations.

## Why do we have a staging area?
Staging area contains files that are going to be a part of the next commit, which lets git know what changes in the file are going to occur for the next commit.

Staging area helps us in the following ways.
1. To split up one large change into multiple commits.
2. To review changes.
3. To review merge conflicts quickly
4. To commit multiple small changes instead of single large changes which is a good practice.

## How do we install multiple python packages in a text file
To install all packages that is listed in a text file "text.txt",  we may use the following `pip` command
```python
pip install -r text.txt
```
## How do we access methods of the parent class in python
We can use `super()` function to access methods of the parent class.


## References

[Network Topology](https://www.educba.com/types-of-network-topology/)

[if \_\_name\_\_ == '\_\_main\_\_':](https://stackoverflow.com/questions/28336627/if-name-main-python)

[Abstract Classes](https://www.geeksforgeeks.org/abstract-classes-in-python/)

[Abstraction](https://www.faceprep.in/python/abstraction-in-python/)

[Inheritance](https://www.geeksforgeeks.org/inheritance-in-python/)

[Deadlock](https://www.geeksforgeeks.org/introduction-of-deadlock-in-operating-system/)

[Deadlock and Starvation](https://www.geeksforgeeks.org/difference-between-deadlock-and-starvation-in-os/
)

[Distributed and Centralized Version Control System 1](https://www.oshyn.com/blog/version-control-systems-distributed-vs-centralized)

[Distributed and Centralized Version Control System 2](https://www.geeksforgeeks.org/centralized-vs-distributed-version-control-which-one-should-we-choose/)

[Fragmentation and Replication](https://www.dlsweb.rmit.edu.au/Toolbox/knowmang/content/distributed_sys/ddms_design.htm)

[Staging in Git](https://stackoverflow.com/questions/49228209/whats-the-use-of-the-staging-area-in-git)

```
Author: Fahid Latheef A

Date: August 16, 2021
```
